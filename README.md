MUSH

====

### Mush app ###

* Mush is an application to know the musical facet of your city and to share with other people your favorite tastes and places in a quick and intuitive way
* V1.0 Stable

### Dependencies ###
- Mysql
- [NODE.js][node-url]


### Members ###
- David Ferrer Poveda - 2nDAW IES Ausiàs March 


### NODE Install (The apt-get version is 0.10, 4x version is needed) ###
- curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -
- sudo apt-get install nodejs


### DB Structure ###
- The structure is in the ./mush/www/dbdefinition.sql file


### DB Configuration ### 
- Edit ./mush/www/routes/bd.js file for access


### Ways to start server ###
- If the database structure has been implemented, the server can already be started through the file ./mush/www/app.js
- Way 1: $ node ./mush/www/app.js start (The default port is 9999, You can specify the port before starting the server in the file app.js [port variable])
- Way 2: $ forever ./mush/www/app.js start (Trough 'forever' command the server will remain active)


### Demo credentials ###
- User: usu1 & Pass: usu1 
- User: usu2 & Pass: usu2
- User: usu3 & Pass: usu3


### With this version you can: ###
- Publish from a street musician you just found until that festival that you have been waiting for
- Import Meetup events from a group


### Still remain to implement: ###
- Improve publishing options to differentiate a store, festival, or personal event
- Finish the interactive map with filtering options
- Integrate and interleave user comments for a better user experience
- Add more ways to import events from other open Data apps
- Use HTTPs for secure connections


### Contact ###

- a14davferpov@iam.cat



[node-url]: https://nodejs.org/es/