var os=require('os');

console.log('Sistema operativo:'+os.platform());
console.log('Versión del sistema operativo:'+os.release());
console.log('Memoria total:'+os.totalmem()+' bytes');
console.log('Memoria libre:'+os.freemem()+' bytes');

/*
OTHERS: os, fs, http, url, net, path, process, dns etc..

var os=require('os');

console.log('Memoria libre:'+os.freemem());
var vec=[];
for(var f=0;f<1000000;f++) {
    vec.push(f);
}	
console.log('Memoria libre después de crear el vector:'+os.freemem());
*/