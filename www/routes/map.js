var express = require('express');
var router = express.Router();
var bd = require('./bd');
var crypto = require('crypto');

var mysql=require('mysql');


router.get('/', function(req, res, next) {
	console.log("GET [/] -> R: map");
	
	if (req.session.username){
		
		res.render('map', {
			title: "Mushes on Map",
			username: req.session.username,
			useremail: req.session.useremail,
			userimage: req.session.userimagename,
			userimagebg: req.session.userimagebg			
		});
		
	} else {
		res.render('index', {
			title: "Mush Index"
		});
	}
});


module.exports = router;