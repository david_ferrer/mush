var express = require('express');
var bd = require('./bd');
var router = express.Router();
var crypto = require('crypto');

var mysql=require('mysql');

router.get('/', function(req, res, next) {
	console.log("GET [/] -> R: createaccount");
	
	res.render('createaccount', {title:"Create account"});
});

router.post('/', function(req, res, next) {
	console.log("POST [/createaccount] -> R: createaccount");
	
	name = req.body.name;
	surname = req.body.surname;
	username = req.body.username;
	password = req.body.password;
	phone = req.body.phone;
	email = req.body.email;
	
	var errors = "";
	var allOK = true;
	
	// Check for create account
	
	// If username is already in use
	// SQL INJECTION PREVENT
	var sql = "SELECT username FROM ?? WHERE ?? = ?";
	var inserts = ['users', 'username', username];
	sql = mysql.format(sql, inserts);
		
	console.log("SQL: " + sql);
	
	bd.query(sql,
		[],
		function(error,filas){
			if (error) {
				
				errorMsg = 'error en la consulta';
				console.log(errorMsg);
				
				res.render('error', {errorMessage: errorMsg});
				
			} else if (filas.length>0 && filas.length==1) {
				console.log("SI existeix a la BD");
				console.log("SI MySQL - " + username);
				
				allOK = false;
				errors += "Username is already in use <br>";
			} /* end of USERNAME SQL */
		
	// If phone is already in use
	// SQL INJECTION PREVENT
	var sql = "SELECT phone FROM ?? WHERE ?? = ?";
	var inserts = ['users', 'phone', phone];
	sql = mysql.format(sql, inserts);

	console.log("SQL: " + sql);

	bd.query(sql,
		[],
		function(error,filas){
			if (error) {
				console.log('error en la consulta');
			} else if (filas.length>0 && filas.length==1) {
						
				allOK = false;
				errors += "Phone is already in use <br>";
	
			} /* end of PHONE SQL */

	// If email is already in use
	// SQL INJECTION PREVENT
	var sql = "SELECT email FROM ?? WHERE ?? = ?";
	var inserts = ['users', 'email', email];
	sql = mysql.format(sql, inserts);
				
	console.log("SQL: " + sql);
				
	bd.query(sql,
		[],
		function(error,filas){
			if (error) {
				console.log('error en la consulta');
			} else if (filas.length>0 && filas.length==1) {
						
				allOK = false;
				errors += "Email is already in use <br>";
				
			} /* end of EMAIL SQL */

	if (allOK){
		// If can insert
		// SQL INJECTION PREVENT
		var sql = "INSERT INTO ?? (??, ??, ??, ??, ??, ??) VALUES (?, ?, ?, ?, ?, ?)";
		var inserts = ['users', 'username', 'password','name', 'surname', 'phone', 'email', username, md5(password), name, surname, phone, email];
		sql = mysql.format(sql, inserts);
									
		console.log("SQL: " + sql);
		
		bd.query(sql,
			[],
			function(error,filas){
				if (!error) {
					console.log('CA SUCCESS _console');
														
					res.render('info', {
						urlReturn: "/",
						title:"CA | Success",
						message:"Account created",
					});
				} /* end of INSERT SQL*/
			}
		);
		 
	} else {
		console.log('CA ERROR _console');
		
		res.render('createaccount', {
			title:"CA | Failure",
			error:errors,
			val_name:name,
			val_surname:surname,
			val_username:username,
			val_phone:phone,
			val_email:email 
		});
		
	}
	})})});
	
});
function md5(string) {
  return crypto.createHash('md5').update(string).digest('hex');
}

module.exports = router;