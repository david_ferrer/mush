var express = require('express');
var router = express.Router();
var bd = require('./bd');
var crypto = require('crypto');

var mysql=require('mysql');


router.get('/', function(req, res, next) {
	console.log("GET [/] -> R: login");
	
	if (req.session.username){
		res.writeHead(302, {'Location': '/page'});
		res.end();
	} else {
		res.render('index', {
			title: "Mush Index"
		});
	}
});


router.post('/login', function(req, res, next) {
	console.log("POST [/login] -> R: login");
	
	username = req.body.username;
	password = req.body.password;
	encrPassword = md5(password);
	
	console.log("Nom: " + username);
	console.log("Pass: " + encrPassword);


	
		
	if (req.session.username){
		res.writeHead(302, {'Location': '/page'});
		res.end();
	} else {
		
		// SQL INJECTION PREVENT
		var sql = "SELECT * FROM ?? WHERE ?? = ? AND ?? = ?";
		var inserts = ['users', 'username', username, "password", encrPassword];
		sql = mysql.format(sql, inserts);
		
		console.log("SQL: " + sql);
		
		bd.query(sql,
			[],
			function(error,filas){
				if (error) {
					errorMsg = 'error en la consulta';
					console.log(errorMsg);
					
					res.render('error', {errorMessage: errorMsg});
				} else if (filas.length>0 && filas.length==1) {
					
					userid = filas[0].id;
					req.session.userid = userid;
					
					username = filas[0].username;
					req.session.username = username;
					
					useremail = filas[0].email;
					req.session.useremail = useremail;
					
					userimagename = filas[0].userimagename;
					req.session.userimagename = userimagename;
					
					userimagebg = filas[0].userimagebg; 
					req.session.userimagebg = userimagebg; 
					
					console.log("ID: " + userid);
					console.log("Username: " + username);
					console.log("UserImageName: " + userimagename);
					console.log("UserImageBg: " + userimagebg);
					
					res.writeHead(302, {'Location': '/page'});
					res.end();
				} else {
					res.render('index', {
						title : "Login Failure",
						error : "<i style='color:red;'>Your username/password was incorrect</i>"
					});
				}
			});
	}	
});

function md5(string) {
  return crypto.createHash('md5').update(string).digest('hex');
}

module.exports = router;
