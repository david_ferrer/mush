var express = require('express');
var router = express.Router();
var bd = require('./bd');
var crypto = require('crypto');
var meetup = require('meetup-api')({
	key: 'e42184a361d52366c5c2d106a5db72'
});

var mysql=require('mysql');

router.get('/', function(req, res, next) {
	console.log("GET [/] -> R: meetup");
	
	if (req.session.username){
		
		var eventsArrayObj = [];
		
		var sql = "SELECT * FROM ?? WHERE iduser = ?";
		var inserts = ['meetups', req.session.userid];
		sql = mysql.format(sql, inserts);
		
		console.log("SQL: " + sql);
		
		bd.query(sql,
			[],
			function(error,filas,fields){
				if (error) {
					
					errorMsg = 'error en la consulta';
					console.log(errorMsg);
					
					res.render('error', {errorMessage: errorMsg});
					
				} else {
					var aux = 0;
					filas.forEach(function () {
						
						eventsArrayObj.push({
							id:filas[aux].id,
							iduser:filas[aux].iduser,
							meetupid:filas[aux].meetupid,
							name:decodeURI(filas[aux].name),
							groupname:decodeURI(filas[aux].groupname),
							description:decodeURI(filas[aux].description),
							country:filas[aux].country,
							countryname:filas[aux].countryname,
							city:filas[aux].city,
							price:filas[aux].price,
							accepts:filas[aux].accepts,
							descriptionfee:decodeURI(filas[aux].descriptionfee),
							currency:filas[aux].currency,
							address:decodeURI(filas[aux].address),
							lat:filas[aux].lat,
							lng:filas[aux].lng,
							url:decodeURI(filas[aux].url),
							imagesrc:decodeURI(filas[aux].imagesrc),
							visibility:filas[aux].visibility
						});
						
						aux++;
						
					}); /* end of forEach */ 
				}
				
				var eventsLen = eventsArrayObj.length;
				console.log("Array length: " + eventsLen);
			
				if (eventsArrayObj.length==0) {
					console.log("No existeix cap meetup en la BD");
						
					res.render('page', {
						title: "Meetups List",
						eventsListNotFound: "No existeix cap meetup en la BD",
						username: req.session.username,
						useremail: req.session.useremail,
						userimage: req.session.userimagename,
						userimagebg: req.session.userimagebg
					});
		
				} else {
					console.log("S'han trobat events en la BD");
					
					var eventsList = "";
					
					for(i=0; i<eventsLen; i++){
					
					eventsList+= "<div class='event-item'>";
					
					eventsList+= "			<div class='event_image'>";
					if (
						eventsArrayObj[i].imagesrc != "" &&
						eventsArrayObj[i].imagesrc != "undefined" &&
						eventsArrayObj[i].imagesrc != null &&
						eventsArrayObj[i].imagesrc.startsWith("http")
					) 
						eventsList+= "				<img class='materialboxed my-image-style-eventslist' src='" + eventsArrayObj[i].imagesrc + "'></img>";
					else
						eventsList+= "				<img class='materialboxed my-image-style-eventslist' src='images/event_images/meetup.png'></img>";
					eventsList+= "			</div>";
					
					eventsList+= "			<div class='event_info teal lighten-1 my-small-padding'>";

													// Name
					eventsList+= "				<span class='mupName'>" + eventsArrayObj[i].name + "</span>";
					eventsList+= "				Group Name<span class='mupGroupname'>[" + eventsArrayObj[i].groupname + "]</span>";					
					
					eventsList+= '				<ul class="collapsible popout" data-collapsible="accordion">';
					
													// General Info Collapsiable
					eventsList+= '					<li>';
					eventsList+= '						<div class="collapsible-header">';
					eventsList+= '							<i class="material-icons">subject</i>General info';
					eventsList+= '						</div>';
					
					eventsList+= '						<div class="collapsible-body white my-small-padding">';
					
															// Name
					eventsList+= '							<div class="input-field">';
					eventsList+= '								<i id="icon_name" class="material-icons prefix">label</i>';
					eventsList+= '								<input id="show_name" type="text" readonly="true" value="' + eventsArrayObj[i].name + '\">';
					eventsList+= '							</div>';
					
															// Countryname
					eventsList+= '							<div class="input-field">';
					eventsList+= "								<i class='material-icons prefix'>&#xE894;</i>";
					eventsList+= '								<input id="show_countryname" name="countryname" readonly="true" value="' + eventsArrayObj[i].countryname + '\">';
					eventsList+= '							</div>';
					
															// City
					eventsList+= '							<div class="input-field">';
					eventsList+= "								<i class='material-icons prefix'>&#xE7F1;</i>";
					eventsList+= '								<input id="show_city" name="city" readonly="true" value="' + eventsArrayObj[i].city + '\">';
					eventsList+= '							</div>';
					
															// Address
					eventsList+= "							<div class='aligned'>";
					eventsList+= "								<i class='material-icons'>room</i>";
					eventsList+= "								<span class='mupAddress'>" + eventsArrayObj[i].address + "</span>";
					eventsList+= "							</div>";
					
															// Url
					eventsList+= "							<div class='aligned'>";
					eventsList+= "								<i class='material-icons'>&#xE157;</i>";
					eventsList+= "								<a href='" + eventsArrayObj[i].url + "'> <span class='mupUrl'>" + eventsArrayObj[i].url + "</span> </a>";
					eventsList+= "							</div>";					

															// Visibility
					eventsList+= '							<div class="input-field">';
					eventsList+= '								<i id="icon_visibility" class="material-icons prefix my-left">visibility</i>';
					eventsList+= '								<input id="show_visibility" name="visibility" readonly="true" type="text" value="' + eventsArrayObj[i].visibility + '\">';
					eventsList+= '								<label for="show_visibility">Visibility</label>';
					eventsList+= '							</div>';				
								
					eventsList+= '						</div> <!-- end Body general ifno -->';
					eventsList+= '					</li> <!-- end General Info Collapsiable -->';
					
													// Location Collapsiable
					if ( eventsArrayObj[i].address != "" && eventsArrayObj[i].lat != 0 && eventsArrayObj[i].lng != 0){
						eventsList+= '		<li>';
						eventsList+= '			<div class="collapsible-header">';
						eventsList+= '				<i class="material-icons">location_on</i>Location';
						eventsList+= '			</div>';
						eventsList+= '			<div class="collapsible-body no-padding">';
						eventsList+= '				<iframe class="map-event-dynamic my-no-border" src="http://maps.google.com/maps?q=' + eventsArrayObj[i].lat + ',' + eventsArrayObj[i].lng + '&z=15&output=embed"></iframe>';
						eventsList+= '			</div>';
						eventsList+= '		</li>';
					} else {
						eventsList+= '		<li>';
						eventsList+= '			<div class="collapsible-header">';
						eventsList+= '				<i class="material-icons my-color-grey">location_off</i>Location';
						eventsList+= '			</div>';
						eventsList+= '		</li>';
					}
											
					
					eventsList+= '			<li>';
					eventsList+= '				<div class="collapsible-header">';
					eventsList+= '					<i class="material-icons">receipt</i>Meetup web description';
					eventsList+= '				</div>';
					eventsList+= '				<div class="collapsible-body no-padding">';
					eventsList+= '					<div class="container">';
					eventsList+= '						<div class="row">';
					eventsList+= '							<div class="col s12">';
					eventsList+= "								<span class='mupDescription'>" + eventsArrayObj[i].description + "</span>";
					eventsList+= '							</div>';
					eventsList+= '						</div>';
					eventsList+= '					</div>';
					eventsList+= '				</div>';
					eventsList+= '			</li>';
					
					eventsList+= '		</ul>';
					
													// Country name
					eventsList+= "					<div class='aligned'>";
					eventsList+= "						<i class='material-icons'>&#xE894;</i>";
					eventsList+= "						<span class='mupCountryname'>" + eventsArrayObj[i].countryname + "</span>";
					eventsList+= "					</div>";
					
													// City
					eventsList+= "					<div class='aligned'>";
					eventsList+= "						<i class='material-icons'>&#xE7F1;</i>";
					eventsList+= "						<span class='mupCity'>" + eventsArrayObj[i].city + "</span>";
					eventsList+= "					</div>";
					
													//  Price
					eventsList+= "					<div class='aligned'>";
					eventsList+= "						<i class='material-icons'>&#xE926;</i>";
					eventsList+= "						<span class='mup'>" + eventsArrayObj[i].price + " | </span>";
					eventsList+= "						<span class='mup'>" + eventsArrayObj[i].currency + " | </span>";
					eventsList+= "						<span class='mup'>" + eventsArrayObj[i].accepts + " | </span>";
					eventsList+= "						<span class='mup'>" + eventsArrayObj[i].descriptionfee + "</span>";
					eventsList+= "					</div>";
					
													// Address  
					eventsList+= "					<div class='aligned'>";
					eventsList+= "						<i class='material-icons'>room</i>";
					eventsList+= "						<span class='mup'>" + eventsArrayObj[i].address + "</span>";
					eventsList+= "					</div>";

													//  Url
					eventsList+= "					<div class='aligned'>";
					eventsList+= "						<i class='material-icons'>&#xE157;</i>";
					eventsList+= "						<a class='mupUrl' href='" + eventsArrayObj[i].url + "'<span>" + eventsArrayObj[i].url + "</span></a>";
					eventsList+= "					</div>";
				
													// Visibility  
					eventsList+= "					<div class='aligned'>";
					eventsList+= "						<i class='material-icons'>visibility</i>";
					eventsList+= "						<span class='mup'>" + eventsArrayObj[i].visibility + "</span>";
					eventsList+= "					</div>";
					
					
					eventsList+= "		</div> <!-- end event_info -->";
					eventsList+= "</div> <!-- end event_item -->";
										
					} /* end events for */
					
					
		 			res.render('page', {
						title: "Meetups List",
						username: req.session.username,
						useremail: req.session.useremail,
						eventsList: eventsList,
						userimage: req.session.userimagename,
						userimagebg: req.session.userimagebg
					});
				}
			});
			
		} else {
			res.render('index', {
			title: "Mush Index"
		});
		}
		
});

router.post('/create-meetup-group', function(req, res, next) {
	console.log("POST [/create-meetup-group] -> R: meetup");
	
	var insertsOk = true;
	var alreadyExists = false;
	
	if (req.session.username){
			
		// ID
		userid = req.session.userid;
	
		//MEETUP API Passant un grup com a paràmetre (molts meetups)
		var parameters = {
			//id: req.body.specifiedid,
			group_urlname:req.body.meetupurlname
		} 
		
		console.log("Parameters");
		//console.log("ID: " + parameters.id);
		console.log("URL: " + parameters.group_urlname);
		
		meetup.getEvents(parameters, function(err, resp) {
			
			console.log(" - - - - - - - - - -");
			
			if (err){
				console.log("ERROR!");
				
				res.render('info', {
					urlReturn: "/",
					message : "Invalid url"
				});
			} else {
			
				// META Vars
				meta = resp.meta;
				total_count = meta.total_count;
				url = meta.url;
				
				if (total_count == 0) {
					res.render('info', {
						urlReturn: "/",
						message : "No meetups found"
					});
				} else {
				
					
					console.log("Total_count: " + total_count);
					console.log("Url: " + url);
					
					meetups = resp.results;
					
					for (i=0; i<total_count; i++) {
						
						var idname = "", 
						groupname = "", 
						description = "", 
						country = "", 
						countryname = "", 
						city = "", 
						lat = "", 
						lng = "", 
						price = "", 
						accepts = "", 
						descriptionfee = "", 
						currency = "", 
						address = "", 
						visibility = "", 
						url = "",
						imagesrc = "";
						
						mup = meetups[i];
						
						if (mup.id) meetupid = mup.id;
						if (mup.name) name = encodeURI(mup.name);
						
						if (mup.group)
							if (mup.group.name) groupname = encodeURI(mup.group.name);
							
						if (mup.description){
							// We try to get the first SRC image from description to put like image_event
							auxdescription = mup.description;
							var m, urls = [], rex = /<img[^>]+src="?([^"\s]+)"?\s*\/>/g;
							
							while ( m = rex.exec( auxdescription ) ) {
								urls.push( m[1] );
							}
							
							imagesrc = urls[0];
							console.log("URLs: " + urls );
							
							description = encodeURI(auxdescription);
						}
						
						if (mup.venue){
							if (mup.venue.country) country = mup.venue.country;
							if (mup.venue.localized_country_name) countryname = encodeURI(mup.venue.localized_country_name);
							if (mup.venue.city) city = encodeURI(mup.venue.city);
							if (mup.venue.lat) lat = mup.venue.lat;
							if (mup.venue.lon) lng = mup.venue.lon;
						}
						
						if (mup.fee){
							if (mup.fee.amount) price = parseFloat( parseInt(mup.fee.amount) ); 
							if (mup.fee.accepts)	accepts = mup.fee.accepts; 
							if (mup.fee.description) descriptionfee = mup.fee.description
							if (mup.fee.currency) currency = mup.fee.currency;
						}
							
						if (mup.how_to_find_us) address = encodeURI(mup.how_to_find_us);
													
						if (mup.visibility) visibility = mup.visibility;
						if (mup.event_url) url = encodeURI(mup.event_url);
						
						console.log(" - - - - - - - - - - MEETUP - - - - - - - - -");
						
						console.log("meetupid: " + meetupid);
						console.log("name: " + name);
						console.log("groupname: " + groupname);
						console.log("description: " + description);
						console.log("country: " + country);
						console.log("countryname: " + countryname);
						console.log("city: " + city);
						console.log("address: " + address);
						console.log("price: " + price + " typeof: [" + typeof price + "]");
						console.log("accepts: " + accepts);
						console.log("descriptionfee: " + descriptionfee);
						console.log("currency: " + currency);
						console.log("lat: " + lat);
						console.log("lng: " + lng);						
						console.log("url: " + url);
						console.log("imagesrc: " + imagesrc);
						console.log("visibility: " + visibility);
						
						console.log(" - - - - - - - - - - - - - - - - - - - - - - -");
						
						// La sentència SQL que utilitzarem serà [SQLfinal] 
						// perquè sempre em salta errors quan utilitzo el mètode mysql.format() 
						// per controlar les injeccions en la consulta.
						// Potser té a veure amb els símbols '/' de les rutes o que codifico tot amb encodeURI()
						// i dóna error. Com no sé com solucionar-ho ho deixo estar.
						// Llençem contra la BD la consulta tal cual.
						SQLfinal = "";
						SQLfinal += "INSERT INTO meetups (id, iduser, meetupid, name, groupname, description, country, countryname, city, price, accepts, descriptionfee, currency, address, lat, lng, url, imagesrc, visibility) VALUES (";
						SQLfinal += null 					+ ", ";
						SQLfinal += userid 				+ ", \"";
						SQLfinal += meetupid 			+ "\", \"";
						SQLfinal += name					+ "\", \"";
						SQLfinal += groupname			+ "\", \"";
						SQLfinal += description			+ "\", \"";
						SQLfinal += country				+ "\", \"";
						SQLfinal += countryname			+ "\", \"";
						SQLfinal += city 					+ "\", ";
						
						if (typeof price == "string")
							SQLfinal += 					"0.00, \"";
						else
							SQLfinal += price				+ ", \"";
							
						SQLfinal += accepts				+ "\", \"";
						SQLfinal += descriptionfee		+ "\", \"";
						SQLfinal += currency				+ "\", \"";
						SQLfinal += address				+ "\", \"";
						SQLfinal += lat					+ "\", \"";
						SQLfinal += lng					+ "\", \"";
						SQLfinal += url					+ "\", \"";
						
						if (imagesrc == null)
							SQLfinal += 					"\", \"";
						else
							SQLfinal += imagesrc				+ "\", \"";
												
						SQLfinal += visibility			+ "\")";
						
						console.log("SQL: " + SQLfinal);
						
						bd.query(SQLfinal,
							[],
							function(error,filas){
								if (error) {
									insertsOk = false;
									console.log('Error en input Meetup group [' + name + ']: ' + error);
								} else {
									console.log('Success en input Meetup group [' + name + ']');
								}
							});						
							
					} /* end for */
					
				} /* end total_count > 0 */
			} /* end else ERROR */
		}); /* end meeupt.GetEvents() */
		
		
		if (alreadyExists) {
			res.render('error',{
				errorMessage: "Some meetups from that group`already exists"
			});
		} else if (insertsOk) {
			res.render('info',{
				urlReturn : "/mymeetups",
				message: "Meetups imported correctly"
			});
		} else {
			res.render('error',{
				errorMessage: "Errors while importing"
			});
		}
	
	} else {
		res.render('index', {
			title: "Mush Index"
		});
	}

});

router.post('/create-meetup-specified', function(req, res, next) {
	console.log("POST [/create-meetup-specified] -> R: meetup");
	
	var insertsOk = true;
	var alreadyExists = false;
	
	if (req.session.username){
			
		// ID
		userid = req.session.userid;
	
		//MEETUP API Passant un grup i id específic com a paràmetre (un únic meetup)
		var parameters = {
			id: req.body.specifiedid,
			group_urlname: req.body.specifiedurlname
		} 
		
		console.log("Parameters");
		console.log("ID: " + parameters.id);
		console.log("URL: " + parameters.group_urlname);
		
		meetup.getEvent(parameters, function(err, resp) {
			
			console.log(" - - - - - - - - - -");
			
			if (err){
				console.log("ERROR!: " + err);
				
				res.render('info', {
					urlReturn: "/",
					message : "Invalid url"
				});
			} else {
							
				if (resp.errors) {
					res.render('info', {
						urlReturn: "/",
						message : "No meetups found"
					});
				} else {
				
					
					console.log("Response: " + resp);
					
					mup = resp;
					
					var idname = "", 
					groupname = "", 
					description = "", 
					country = "", 
					countryname = "", 
					city = "", 
					lat = "", 
					lng = "", 
					price = "", 
					accepts = "", 
					descriptionfee = "", 
					currency = "", 
					address = "", 
					visibility = "", 
					url = "",
					imagesrc = "";
					
					if (mup.id) meetupid = mup.id;
					if (mup.name) name = encodeURI(mup.name);
					
					if (mup.group)
						if (mup.group.name) groupname = encodeURI(mup.group.name);
						
					if (mup.description){
						// We try to get the first SRC image from description to put like image_event
						auxdescription = mup.description;
						var m, urls = [], rex = /<img[^>]+src="?([^"\s]+)"?\s*\/>/g;
						
						while ( m = rex.exec( auxdescription ) ) {
							urls.push( m[1] );
						}
						
						imagesrc = urls[0];
						console.log("URLs: " + urls );
						
						description = encodeURI(auxdescription);
					}
					
					if (mup.venue){
						if (mup.venue.country) country = mup.venue.country;
						if (mup.venue.localized_country_name) countryname = encodeURI(mup.venue.localized_country_name);
						if (mup.venue.city) city = encodeURI(mup.venue.city);
						if (mup.venue.lat) lat = mup.venue.lat;
						if (mup.venue.lon) lng = mup.venue.lon;
					}
					
					if (mup.fee){
						if (mup.fee.amount) price = parseFloat( parseInt(mup.fee.amount) ); 
						if (mup.fee.accepts)	accepts = mup.fee.accepts; 
						if (mup.fee.description) descriptionfee = mup.fee.description
						if (mup.fee.currency) currency = mup.fee.currency;
					}
						
					if (mup.how_to_find_us) address = encodeURI(mup.how_to_find_us);
												
					if (mup.visibility) visibility = mup.visibility;
					if (mup.event_url) url = encodeURI(mup.event_url);
					
					console.log(" - - - - - - - - - - MEETUP - - - - - - - - -");
					
					console.log("meetupid: " + meetupid);
					console.log("name: " + name);
					console.log("groupname: " + groupname);
					console.log("description: " + description);
					console.log("country: " + country);
					console.log("countryname: " + countryname);
					console.log("city: " + city);
					console.log("address: " + address);
					console.log("price: " + price + " typeof: [" + typeof price + "]");
					console.log("accepts: " + accepts);
					console.log("descriptionfee: " + descriptionfee);
					console.log("currency: " + currency);
					console.log("lat: " + lat);
					console.log("lng: " + lng);						
					console.log("url: " + url);
					console.log("imagesrc: " + imagesrc);
					console.log("visibility: " + visibility);
					
					console.log(" - - - - - - - - - - - - - - - - - - - - - - -");
					
					// La sentència SQL que utilitzarem serà [SQLfinal] 
					// perquè sempre em salta errors quan utilitzo el mètode mysql.format() 
					// per controlar les injeccions en la consulta.
					// Potser té a veure amb els símbols '/' de les rutes o que codifico tot amb encodeURI()
					// i dóna error. Com no sé com solucionar-ho ho deixo estar.
					// Llençem contra la BD la consulta tal cual.
					SQLfinal = "";
					SQLfinal += "INSERT INTO meetups (id, iduser, meetupid, name, groupname, description, country, countryname, city, price, accepts, descriptionfee, currency, address, lat, lng, url, imagesrc, visibility) VALUES (";
					SQLfinal += null 					+ ", ";
					SQLfinal += userid 				+ ", \"";
					SQLfinal += meetupid 			+ "\", \"";
					SQLfinal += name					+ "\", \"";
					SQLfinal += groupname			+ "\", \"";
					SQLfinal += description			+ "\", \"";
					SQLfinal += country				+ "\", \"";
					SQLfinal += countryname			+ "\", \"";
					SQLfinal += city 					+ "\", ";
					
					if (typeof price == "string")
						SQLfinal += 					"0.00, \"";
					else
						SQLfinal += price				+ ", \"";
						
					SQLfinal += accepts				+ "\", \"";
					SQLfinal += descriptionfee		+ "\", \"";
					SQLfinal += currency				+ "\", \"";
					SQLfinal += address				+ "\", \"";
					SQLfinal += lat					+ "\", \"";
					SQLfinal += lng					+ "\", \"";
					SQLfinal += url					+ "\", \"";
					
					if (imagesrc == null)
						SQLfinal += 					"\", \"";
					else
						SQLfinal += imagesrc				+ "\", \"";
											
					SQLfinal += visibility			+ "\")";
					
					console.log("SQL: " + SQLfinal);
					
					bd.query(SQLfinal,
						[],
						function(error,filas){
							if (error) {
								insertsOk = false;
								console.log('Error en input Meetup group [' + name + ']: ' + error);
							} else {
								console.log('Success en input Meetup group [' + name + ']');
							}
						});
				} /* end else ERROR */
			}
		}); /* end meeupt.GetEvent() */
		
		
		if (alreadyExists) {
			res.render('error',{
				errorMessage: "Some meetups from that group`already exists"
			});
		} else if (insertsOk) {
			res.render('info',{
				urlReturn : "/mymeetups",
				message: "Meetups imported correctly"
			});
		} else {
			res.render('error',{
				errorMessage: "Errors while importing"
			});
		}
	
	} else {
		res.render('index', {
			title: "Mush Index"
		});
	}

});


module.exports = router;