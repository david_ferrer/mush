var express = require('express');
var router = express.Router();
var bd = require('./bd');
var crypto = require('crypto');

var mysql=require('mysql');

var meetup = require('../node_modules/meetup-api/lib/meetup')({
	key: 'e42184a361d52366c5c2d106a5db72'
});

/* GET home page. */
router.get('/', function(req, res, next) {
	console.log("GET [/] -> R: index");
	
	if (req.session.username){
		res.writeHead(302, {'Location': '/page'});
		res.end();
	} else
		res.render('index', {
			title: "Mush Index"
		});
});

function md5(string) {
  return crypto.createHash('md5').update(string).digest('hex');
}

module.exports = router;
