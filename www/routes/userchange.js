var express = require('express');
var router = express.Router();
var bd = require('./bd');
var crypto = require('crypto');
var fs = require('fs');
var path = require('path')

var tinify = require("tinify");
tinify.key = "dJe0XpToN0Lz2sNpzLd96kupc2RcTpKI";

var mysql = require('mysql');


/* USER IMAGE POST */
router.post('/changeuserimage', function(req, res, next) {
	console.log("POST [/] -> R: userchange");
	
	if (req.session.username){
		
		// Image
		var validImagePath = true;
		var filepath = req.files.image.path;
		var fileextension = path.extname(filepath);
		if (fileextension === ""){
			errors += "Fileextension error: [" + fileextension + "] \n";
			fileextension = null;
			allOK = false;
			validImagePath = false;
		}
		
		if (validImagePath){
		   
	   	// ID image name will be [SECONDS.EXTENSION]
			// Path to know where save image
   		var imageFolder = '/public/images/user_images/';
   		var imageFolderToWrite = "public/images/user_images/"
   		var imageFolderToImgTag = '/images/user_images/';
	   	var d = new Date();
	   	var uniqueSecondsNumber = d.getTime();
	   	var imageName = uniqueSecondsNumber + fileextension;
		   
	   	// Image Path [TIME+EXTENSION]
	   	var imagePath = imageName;
		
			console.log("IS → image TMP path: " + filepath);
			console.log("OS → image path to write: " + imageFolderToWrite + imagePath);
			
	   	var is = fs.createReadStream(filepath);
	   	var os = fs.createWriteStream(imageFolderToWrite + imagePath);
		
	   	is.pipe(os);	   	
		
	   	is.on('end', function() {
	      	//eliminamos el archivo temporal
      		fs.unlinkSync(filepath);
   		});
   		
   		//  - - - - - Tiny png API uses - - - - - //
   		// Simplement li diem que agafi la imatge que s'acaba de guardar
   		// li apliqui el format de compressió i el dessi amb el mateix PATH
   		var source = tinify.fromFile(imageFolderToWrite + imagePath);
   		source.toFile(imageFolderToWrite + imagePath);
   		//  - - - - - - - - - - - - - - - - - - - //
   		
   				
			// SQL INJECTION PREVENT
			var sql = "UPDATE ?? SET ?? = ? WHERE id = ?";
			var inserts = ['users', 'userimagename', imageName, req.session.userid];
			sql = mysql.format(sql, inserts);
			
			console.log("SQL: " + sql);
			
			bd.query(sql,
				[],
				function(error,filas){
					if (error) {
						errorMsg = 'error en la consulta';
						console.log(errorMsg);
						
						res.render('error', {errorMessage: errorMsg});
					} else {
						
						// Comprovem que l'antiga imatge existeix per a borrar-la si és diferent al Cheewaka
						// I indiquem a la sessió [req.session.userimagename] la nova imatge d'usuari
						oldUserImage = req.session.userimagename;
						console.log("Comprovem si existeix: [" + imageFolderToWrite + oldUserImage + "]");
						
						if (fs.existsSync(imageFolderToWrite + oldUserImage)) {
							console.log("Existeix i l'esborrem: [" + imageFolderToWrite + oldUserImage + "]");
							if (oldUserImage != "no-image-chewee.png")
								fs.unlink(imageFolderToWrite + oldUserImage);
						} else {
							console.log("No existeix");
						}
												
						req.session.userimagename = imageName;
						
						res.render('info', {
							title : "User image success",
							message: "User image success",
							urlReturn: "/"
						});
					}
			});
   		
	   } else {
   		imageName = null;
   	}
		
	} else
		res.render('index', {
			title: "Mush Index"
		});
});


/* USER BACKGROUND IMAGE POST */
router.post('/changeuserbgimage', function(req, res, next) {
	console.log("POST [/] -> R: userchange");
	
	if (req.session.username){
		
		// Image
		var validImagePath = true;
		var filepath = req.files.image.path;
		var fileextension = path.extname(filepath);
		if (fileextension === ""){
			errors += "Fileextension error: [" + fileextension + "] \n";
			fileextension = null;
			allOK = false;
			validImagePath = false;
		}
		
		if (validImagePath){
			
	   	// ID image name will be [SECONDS.EXTENSION]
			// Path to know where save image
   		var imageFolder = '/public/images/user_images/';
   		var imageFolderToWrite = "public/images/user_images/"
   		var imageFolderToImgTag = '/images/user_images/';
	   	var d = new Date();
	   	var uniqueSecondsNumber = d.getTime();
	   	var imageName = uniqueSecondsNumber + fileextension;
		   
	   	// Image Path [TIME+EXTENSION]
	   	var imagePath = imageName;
		
			console.log("IS → image TMP path: " + filepath);
			console.log("OS → image path to write: " + imageFolderToWrite + imagePath);
			
	   	var is = fs.createReadStream(filepath);
	   	var os = fs.createWriteStream(imageFolderToWrite + imagePath);
		
	   	is.pipe(os);	   	
		
	   	is.on('end', function() {
	      	//eliminamos el archivo temporal
      		fs.unlinkSync(filepath);
   		});
   		
   		//  - - - - - Tiny png API uses - - - - - //
   		// Simplement li diem que agafi la imatge que s'acaba de guardar
   		// li apliqui el format de compressió i el dessi amb el mateix PATH
   		var source = tinify.fromFile(imageFolderToWrite + imagePath);
   		source.toFile(imageFolderToWrite + imagePath);
   		//  - - - - - - - - - - - - - - - - - - - //
   		
   				
			// SQL INJECTION PREVENT
			var sql = "UPDATE ?? SET ?? = ? WHERE id = ?";
			var inserts = ['users', 'userimagebg', imageName, req.session.userid];
			sql = mysql.format(sql, inserts);
			
			console.log("SQL: " + sql);
			
			bd.query(sql,
				[],
				function(error,filas){
					if (error) {
						errorMsg = 'error en la consulta';
						console.log(errorMsg);
						
						res.render('error', {errorMessage: errorMsg});
					} else {
						
						// Comprovem que l'antiga imatge existeix per a borrar-la si és diferent al Cheewaka
						// I indiquem a la sessió [req.session.userimagename] la nova imatge d'usuari
						oldUserBGImage = req.session.userimagebg;
						console.log("Comprovem si existeix: [" + imageFolderToWrite + oldUserBGImage + "]");
						
						if (fs.existsSync(imageFolderToWrite + oldUserBGImage)) {
							console.log("Existeix i l'esborrem: [" + imageFolderToWrite + oldUserBGImage + "]");
							if (oldUserBGImage != "no-bg-image-festival.jpg")
								fs.unlink(imageFolderToWrite + oldUserBGImage);
						} else {
							console.log("No existeix");
						}
												
						req.session.userimagebg = imageName;
						
						res.render('info', {
							title : "User background image success",
							message: "User background image success",
							urlReturn: "/"
						});
					}
			});
   		
   		
   		
	   } else {
   		imageName = null;
   	}
   	
		
	} else
		res.render('index', {
			title: "Mush Index"
		});
});


module.exports = router;
