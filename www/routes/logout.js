var express = require('express');
var router = express.Router();

/* GET logout page. */
router.get('/', function(req, res, next) {
	console.log("GET [/] -> R: logout");
	
	console.log("borrem: " + req.session.username );
	req.session.destroy();
	res.redirect('/');
});

module.exports = router;