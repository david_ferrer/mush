DROP DATABASE IF EXISTS a14davferpo_mush;
CREATE DATABASE a14davferpo_mush;
USE a14davferpo_mush;

CREATE TABLE users (
	id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	username VARCHAR(30) NOT NULL UNIQUE,
	password VARCHAR(32) NOT NULL,
	name VARCHAR(30),
	surname VARCHAR(30),
	phone INT(9) UNIQUE,
	email VARCHAR(40) UNIQUE,
	userimagename TEXT,
	userimagebg TEXT,
	PRIMARY KEY (id) 
)Engine=InnoDB;
INSERT INTO users (username, password, name, surname, phone, email, userimagename, userimagebg) VALUES 
	('usu1', md5('usu1'), 'David 1', 'Ferrer 1', '111111111', 'usu1@usu1.com', 'no-image-chewee.png', 'no-bg-image-festival.jpg'),
	('usu2', md5('usu2'), 'David 2', 'Ferrer 2', '222222222', 'usu2@usu2.com', 'no-image-chewee.png', 'no-bg-image-festival.jpg'),
	('usu3', md5('usu3'), 'David 3', 'Ferrer 3', '333333333', 'usu3@usu3.com', 'no-image-chewee.png', 'no-bg-image-festival.jpg');


DROP TABLE IF EXISTS events;
CREATE TABLE events(
	id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	iduser INT UNSIGNED NOT NULL,
	name TEXT NOT NULL,
	description TEXT,
	startdate DATE,
	enddate DATE,
	createddate DATETIME,
	capacity INT,
	lat VARCHAR(255),
	lng VARCHAR(255),
	category ENUM('blues', 'dance', 'disco', 'experimental', 'folk', 'funk', 'electronic', 'hip-hip', 'jazz', 'pop', 'latin', 'punk', 'reggae', 'soul', 'urban', 'rock', 'heavy-metal', 'trash'),
	address VARCHAR(255),
	age ENUM('All public', '+3', '+7', '+12', '+16', '+18'),
	imagename TEXT,
	price DOUBLE(5,2),
	phone INT(12),
	web VARCHAR(255),
	email VARCHAR(255),
	visibility ENUM('public', 'private'),
	PRIMARY KEY (id, iduser),
	FOREIGN KEY (iduser) REFERENCES users (id)
)Engine=InnoDB;

DROP TABLE IF EXISTS meetups;
CREATE TABLE meetups(
	id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	iduser INT UNSIGNED NOT NULL,
	meetupid VARCHAR(32) NOT NULL,
	name TEXT,
	groupname TEXT,
	description TEXT,
	country VARCHAR(12),
	countryname VARCHAR(64),
	city VARCHAR(64),
	price DOUBLE(5,2),
	accepts VARCHAR(255),
	descriptionfee VARCHAR(255),
	currency VARCHAR(10),
	address TEXT,
	lat VARCHAR(255),
	lng VARCHAR(255),
	url VARCHAR(255) NOT NULL,
	imagesrc TEXT,
	visibility ENUM('public', 'private'),
	PRIMARY KEY (id, iduser),
	FOREIGN KEY (iduser) REFERENCES users (id),
	UNIQUE (iduser, url)
)Engine=InnoDB;


INSERT INTO events VALUES	(
	1, 1,	"- 1 usu - Sónar 2017", "Descript Sónar 2017", '2017/6/15', '2017/6/20', '2017-05-10 10:10:10'
	, 0, "41.3741689", "2.149066", "experimental", 
	"Avinguda Rius i Taulet, s/n 08004 Barcelona", 'All public',  "sonar.png", 180.00, 
	"111111111", "https://www.facebook.com/sonarfestival",
	"sonar@sonar.es", "public");
	
INSERT INTO events VALUES	(
	1, 2,	"- 2 usu - Sónar 2017", "Descript Sónar 2017", '2017/6/15', '2017/6/20', '2017-05-10 10:10:10'
	, 0, "41.3741689", "2.149066", "experimental", 
	"Avinguda Rius i Taulet, s/n 08004 Barcelona", 'All public',  "sonar.png", 180.00, 
	"111111111", "https://www.facebook.com/sonarfestival",
	"sonar@sonar.es", "public");
	
INSERT INTO events VALUES	(
	1, 3,	"- 3 usu - Sónar 2017", "Descript Sónar 2017", '2017/6/15', '2017/6/20', '2017-05-10 10:10:10'
	, 0, "41.3741689", "2.149066", "experimental", 
	"Avinguda Rius i Taulet, s/n 08004 Barcelona", 'All public',  "sonar.png", 180.00, 
	"111111111", "https://www.facebook.com/sonarfestival",
	"sonar@sonar.es", "public");
