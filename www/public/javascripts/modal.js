$(function () {
	console.log("* modal.js carregat!");
	
	$('.modal').modal();
	
	$("#add-floating").on('click', function () {
		 $('#modal-add').modal('open');
	});
	
	$("#user-image").on('change', checkImage);
	$("#btn-image-form").on('click', checkAndSend);
	
	$("#user-bg-image").on('change', checkImage);
	$("#btn-bg-image-form").on('click', checkBGAndSend);
	
});

// codi trobat a http://jsfiddle.net/4N6D9/1/
var _URL = window.URL || window.webkitURL;
function checkImage(event) {
 	 	
	var file, img;
	var filepath = this.value;
	console.log("filepath: " + filepath);
	var splittedfilepath = filepath.split('.');
	var fileextension = splittedfilepath[splittedfilepath.length - 1];
	 	
   if ((file = this.files[0])) {
    	
        img = new Image();
        img.onload = function() {
        		Materialize.toast("[" + this.width + "x" + this.height + "] px", 1000);
        };
        img.onerror = function() {
        		Materialize.toast("Not a valid image file format: " + file.type, 2000);
        		$("#user-image").val(''); // image input
        		$(".file-path-wrapper input").val(''); // image input filename
        };
        
        img.src = _URL.createObjectURL(file);
    }
 	
}

function checkAndSend() {
	$image = $("#user-image").val();
	
	console.log("Image input: " + $image);
	
	if ($image == "" || $image == "undefined" ){
		Materialize.toast("Invalid input", 2000);
	} else {
		//Materialize.toast("Ok to send", 2000);
		$("#form-change-user-image").submit();
	}
}


function checkBGAndSend() {
	$image = $("#user-bg-image").val();
	
	console.log("BG Image input: " + $image);
	
	if ($image == "" || $image == "undefined" ){
		Materialize.toast("Invalid input", 2000);
	} else {
		//Materialize.toast("Ok to send", 2000);
		$("#form-change-user-bg-image").submit();		    
	}
}