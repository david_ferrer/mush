$(function () {
	console.log("* form-login.js carregat")

	$("#icon_username").addClass("active");
	$("#input_username").addClass("active").focus();
	$("#label_username").addClass("active");
	
	$("#submit-session").on('click', checkSessionFormAndSend);
	
	// si s'apreta la tecla [ENTER] en qualsevol input, el form s'envia
	$("#login-form").find("input").keypress(pressedEnterKeySendSessionForm);
});

function pressedEnterKeySendSessionForm( event ) {
	if ( event.which == 13 ){
		console.log("ENTER");
		event.preventDefault();
		checkSessionFormAndSend();
	}
}

function checkSessionFormAndSend() {
	console.log("check session form");
	$("#login-form").submit();
}
