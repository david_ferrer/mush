var allOkToSendForm = true;

var generalInfoChecks = {
	name : false,
	description : true,
	dates : true,
	capacity : true,
	price : true,
	category : true,
	visibility : true
};

var contactInfoChecks = {
	phone : true,
	web: true,
	email : true
};

var errors = [];

$(function() {
	console.log("* form-create-event.js carregat");
	
   $('select').material_select();
    
   $('#input-description').trigger('autoresize');



// CHECK AND NEW EVENT SUBMISSION //
   $("#submit-create-event").off('click');
   $("#submit-create-event").on('click', checkAndSendNewEventform);
   
   $(".mapsNewEvent").on('click', function () {
   	refreshMap('map-new-event');
   });
   
	// Name check function
   // Nota: no cal indicar els paràmetres que utilitzarà la funció (si n'utilitza)
   $("#input-name").on('keyup', check_name);
   
   // Description check function
   $("#input-description").on('keyup', check_description);
   
   // Start/End date check function
   $("#input-startdate").on('change', check_dates);
   $("#input-enddate").on('change', check_dates);
   
   // Image check function
   $("#input-image").on('change', check_image);
   
   // Phone check function
   $("#input-phone").on('change', check_phone);
   
   // Email check function
   $("#input-email").on('change', check_email);
////////////////////////////////////



// CHECK AND NEW MEETUP SUBMISSION //
	$("#submit-create-meetup-group").on('click', checkAndSendNewMeetupGroup);
/////////////////////////////////////



// CHECK AND NEW SPECIFIED MEETUP SUBMISSION //
	$("#submit-create-meetup-specified").on('click', checkAndSendNewSpecifiedMeetup);
/////////////////////////////////////
   
   
});

function checkAndSendNewEventform() {
	console.log("new event!");
	
	$form = $("#form-create-new-event");

/* DELETE // Name check $input_name = $("#input-name"); */
/* DELETE // Description check	$input_description = $("#input-description"); */
/* DELETE // Start date check	$input_startdate = $("#input-startdate"); */
/* DELETE // End date check $input_enddate = $("#input-enddate"); */
/* DELETE // Capacity check $input_capacity = $("#input-capacity"); */
/* DELETE // Price check $input_price = $("#input-price"); */
/* DELETE // Image check $input_image = $("#input-image"); */
/* DELETE // Phone check $input_phone = $("#input-phone"); */
/* DELETE // Web check $input_web = $("#input-web"); */
/* DELETE // Email check $input_email = $("#input-email"); */	

	if 
	(
		itsAllOkMethod(generalInfoChecks) &&
		itsAllOkMethod(contactInfoChecks)
	)
		$form.submit();
	else
		Materialize.toast("Please check fields before send", 2000);
		
}

function check_name(event) {
	
	var eventName = event.target.value;
		
	if (eventName === ""){
		
		generalInfoChecks.name = false;		
		$(".switchColorGeneral").addClass("my-color-red");
		$("#icon-name").addClass("my-color-red");
  		Materialize.toast("How do people know where to go?", 2000);
  		setTimeout(function () {
  			Materialize.toast("Waiting Howarts's letter?", 2000);
  		},2000);
  		
  	} else if (eventName.length > 32){
  		
  		generalInfoChecks.name = false;  		
  		Materialize.toast("Event name is too long, description fields exists...", 2000);
  		$("#icon-name").addClass("my-color-red");
  		$(".switchColorGeneral").addClass("my-color-red");
  		
  	} else {
  		
  		generalInfoChecks.name = true;
  		$("#icon-name").removeClass("my-color-red");
  		
  		if (itsAllOkMethod(generalInfoChecks))
  			$(".switchColorGeneral").removeClass("my-color-red");
  		else
  			$(".switchColorGeneral").addClass("my-color-red");
  			
  	}

}

function check_description(event) {
	
	var description = event.target.value;
		
	if (description === ""){
		
		generalInfoChecks.description = true;
		if (itsAllOkMethod(generalInfoChecks))
			$(".switchColorGeneral").removeClass("my-color-red");
		
  	} else if (description.length > 1024){
  		
  		generalInfoChecks.description = false;
  		Materialize.toast("Event description is too long (" + description.length +") > 1024 chars", 2000);
  		$("#icon-description").addClass("my-color-red");
  		$(".switchColorGeneral").addClass("my-color-red");
  		
  	} else {
  		
  		generalInfoChecks.description = true;
  		$("#icon-description").removeClass("my-color-red");
  		if (itsAllOkMethod(generalInfoChecks))
	  		$(".switchColorGeneral").removeClass("my-color-red");
	  		
  	}

}
 function check_dates() {
 	
 	var startDate = new Date($('#input-startdate').val());
 	var endDate = new Date($('#input-enddate').val());
 	 	
 	if (startDate > endDate){
 		
 		generalInfoChecks.dates = false;
 		Materialize.toast("I want to meet you, Mr. McFly", 2000);
 		$(".switchColorGeneral").addClass("my-color-red");
 		$("#icon-startdate").addClass("my-color-red");
 		$("#icon-enddate").addClass("my-color-red");
 		console.log("VERMELL");
 		
 	} else {
 		
 		generalInfoChecks.dates = true;
 		if (itsAllOkMethod(generalInfoChecks))
 			$(".switchColorGeneral").removeClass("my-color-red");
 		
 		$("#icon-startdate").removeClass("my-color-red");
		$("#icon-enddate").removeClass("my-color-red");
		
 	}
 
 }
 
// codi trobat a http://jsfiddle.net/4N6D9/1/
var _URL = window.URL || window.webkitURL;
function check_image(event) {
 	 	
	var file, img;
	var filepath = this.value;
	console.log("filepath: " + filepath);
	var splittedfilepath = filepath.split('.');
	var fileextension = splittedfilepath[splittedfilepath.length - 1];
	if (fileextension === ""){
		errors += "Fileextension error: [" + fileextension + "] \n";
		fileextension = null;
		allOK = false;
	}
 	
   if ((file = this.files[0])) {
    	
        img = new Image();
        img.onload = function() {
        	
        		Materialize.toast("[" + this.width + "x" + this.height + "] px", 2000);
        		
        };
        img.onerror = function() {
        		
        		
        		Materialize.toast("Not a valid image file format: " + file.type, 2000);
        		$("#input-image").val(''); // image input
        		$(".file-path-wrapper input").val(''); // image input filename
        		        		
        };
        
        img.src = _URL.createObjectURL(file);
    }
 	
}
 
function check_phone(event) {
	
	var phoneno = /^\d{9}$/;
	var number = event.target.value;
	
	if( number.match(phoneno) || number == ""){
		
		contactInfoChecks.phone = true;
		$("#icon-phone").removeClass("my-color-red");
		if (itsAllOkMethod(contactInfoChecks))
			$(".switchColorContact").removeClass("my-color-red");
		
	} else {
		contactInfoChecks.phone = false;
		$("#icon-phone").addClass("my-color-red");
		$(".switchColorContact").addClass("my-color-red");
	}
	
}

function check_email(event) {

	var re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
	var email = event.target.value;
	
	if ( re.test(email) ){
		
		contactInfoChecks.email = true;
		$("#icon-email").removeClass("my-color-red");
		if (itsAllOkMethod(contactInfoChecks))
			$(".switchColorContact").removeClass("my-color-red");
		
	} else {
		contactInfoChecks.email = false;
		$("#icon-email").addClass("my-color-red");
		$(".switchColorContact").addClass("my-color-red");
	}
}

function itsAllOkMethod(checks) {
 	 	
 	console.log(" - - - VARS - - -");
 	var okVar = true;
 	for (var item in checks){
 		key = item;
 		value = checks[item];
 		if (!value){
 			console.log("[" +	key + "] => " + value + "/ False \\");
 			okVar = false;
 		} else 
 			console.log("[" +	key + "] => " + value);
 			
 	}
 	console.log(" - - - - - - - - -");
 				
 	return okVar;
}

function checkAndSendNewMeetupGroup() {

	$form = $("#form-create-new-meetup-group");
	
	specifiedURL = $form.find("#input-meetup-group").val();
	count = (specifiedURL.match(/\//g) || []).length;
	validStartURL = "https://www.meetup.com/";
	startsWithBool = specifiedURL.startsWith(validStartURL);
	var url_name = "";
	
	console.log("\/ Counts: " + count);
	console.log("Specified URL: " + specifiedURL);
	console.log("StartsWith Correctly: " + startsWithBool);
	console.log("URL name: " + url_name);
	
	
	if (count > 2 && startsWithBool){
		url_name = specifiedURL.split("/")[count - 1];
		
		if (url_name !== ""){
			$form.find("#meetupurlname").val(url_name);
		}
		
		$form.submit();
		
		
	} else if (!startsWithBool)
		Materialize.toast("Url must starts with: [" + validStartURL + "]", 2000);
	else
		Materialize.toast("Please make sure url exists", 2000);
	

} 
 
function checkAndSendNewSpecifiedMeetup() {

	$form = $("#form-create-new-meetup-specified");
	
	specifiedURL = $form.find("#input-meetup-specified").val();
	count = (specifiedURL.match(/\//g) || []).length;
	validStartURL = "https://www.meetup.com/";
	startsWithBool = specifiedURL.startsWith(validStartURL);
	var event_id = "";
	var url_name = "";
	
	console.log("\/ Counts: " + count);
	console.log("Specified URL: " + specifiedURL);
	console.log("StartsWith Correctly: " + startsWithBool);
	
	
	if (count > 2 && startsWithBool){
		url_name = specifiedURL.split("/")[count - 3];
		event_id = specifiedURL.split("/")[count - 1];
		
		console.log("Event id: " + event_id);
		console.log("URL name: " + url_name);
		
		if (event_id !== "" && url_name !== ""){
			$form.find("#specifiedurlname").val(url_name);
			$form.find("#specifiedid").val(event_id);
		}
		
		//form.submit();
		
		
	} else if (!startsWithBool)
		Materialize.toast("Url must starts with: [" + validStartURL + "]", 2000);
	else
		Materialize.toast("Please make sure url exists", 2000);
	
	Materialize.toast("Opció deshabilitada, mètode [getEvents] de l'API no funciona", 5000);
} 
 