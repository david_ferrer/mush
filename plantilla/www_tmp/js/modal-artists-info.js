// GLOBALS
var isApple = false;
var isMobile = false;
var videoSourceWidth;
var videoSourceHeight;
var ratioVideoSourceXY;

  // - - -  Indexes And Preload Images - - -
    var urlIndex = 0;
    // [0] arrayURLs -> Barcelona Guitar Duo
    var arrayUrlBgd = [
      "img/artists/bgd_duo/gallery/bgd1_scalled_tiny.jpg",
      "img/artists/bgd_duo/gallery/bgd2_scalled_tiny.jpg",
      "img/artists/bgd_duo/gallery/bgd3_scalled_tiny.jpg",
      "img/artists/bgd_duo/gallery/bgd4_scalled_tiny.jpg",
      "img/artists/bgd_duo/gallery/bgd5_scalled_tiny.jpg",
    ];

    // [1] arrayURLs -> Alen Garagic
    var arrayUrlAlen =  [
      "img/artists/alen/gallery/a1_scalled_tiny.jpg",
      "img/artists/alen/gallery/a2_scalled_tiny.jpg",
      "img/artists/alen/gallery/a3_scalled_tiny.jpg"
    ];

    // [2] arrayURLs -> Robert Majure
    var arrayUrlRobert = [
      "img/artists/robert/gallery/rm1_tiny.jpg",
      "img/artists/robert/gallery/rm2_tiny.jpg",
      "img/artists/robert/gallery/rm3_tiny.jpg"
    ];

    // [3] arrayURLs -> Joan Benejam
    var arrayUrlJoan = [
      "img/artists/joan/gallery/joan6_scalled_tiny.jpg",
      "img/artists/joan/gallery/joan7_scalled_tiny.jpg",
      "img/artists/joan/gallery/joan8_scalled_tiny.jpg",
      "img/artists/joan/gallery/joan9_scalled_tiny.jpg",
      "img/artists/joan/gallery/joan10_scalled_tiny.jpg",
      "img/artists/joan/gallery/joan11_scalled_tiny.jpg",
      "img/artists/joan/gallery/joan12_scalled_tiny.jpg"
    ];

    var arrayPreloadedImages = [
      arrayImgBgd = new Array(),
      arrayImgAlen = new Array(),
      arrayImgRobert = new Array(),
      arrayImgJoan = new Array()
    ];

    var arrayURLs = [
      arrayUrlBgd,
      arrayUrlAlen,
      arrayUrlRobert,
      arrayUrlJoan
    ];

    for (i = 0; i < arrayURLs.length; i++) {
      for (var j = 0; j < arrayURLs[i].length; j++) {
        arrayPreloadedImages[i][j] = new Image();
        arrayPreloadedImages[i][j].src = arrayURLs[i][j];
      }
    }

    // Set the first image from each artists
    $("#photo-modal-1 .contentImgCarousel").css("background-image",
      "url("+arrayPreloadedImages[0][0].src+")");
    $("#photo-modal-2 .contentImgCarousel").css("background-image",
      "url("+arrayPreloadedImages[1][0].src+")");
    $("#photo-modal-3 .contentImgCarousel").css("background-image",
      "url("+arrayPreloadedImages[2][0].src+")");
    $("#photo-modal-4 .contentImgCarousel").css("background-image",
      "url("+arrayPreloadedImages[3][0].src+")");

    // - - - - - - -  end Preload Images - - - - - - - - -


/* animation a nd transition events*/
var animEndEventNames = {
    'WebkitAnimation' : 'webkitAnimationEnd',
    'OAnimation' : 'oAnimationEnd',
    'msAnimation' : 'MSAnimationEnd',
    'MozAnimation' : 'animationend',
    'animation' : 'animationend'
};

var animEndEventName = 'animationend';

var transEndEventNames = {
    "WebkitTransition" : "webkitTransitionEnd",
    "MozTransition"    : "transitionend",
    "OTransition"      : "oTransitionEnd",
    "msTransition"     : "MSTransitionEnd",
    "transition"       : "transitionend"
};
var transEndEventName = 'transitionend';

$(function() {

	FastClick.attach(document.body);
	detectDevices();

  // safari won't apply correctly parallax effect,
  // then set it on [background-attachment: scroll !important;]
  if (isMobile && isApple)
    $(".parallax").addClass("mobile-fixed-parallax-effect");

  animEndEventName = animEndEventNames[ Modernizr.prefixed('animation') ];
  transEndEventName = transEndEventNames[ Modernizr.prefixed('transition') ];

	$("span.open-biography-artists").on('click', {name: "bio"}, genericShowModal);
  $(".parallax-links .photography").on('click', {name: "photo"}, genericShowModal);
  $("#artists .buy-tickets").on('click', {name: "tickets"}, genericShowModal);
  $(".nav li").has(".fa-language").on('click', {name: "lang"}, genericShowModal);
  $("span.close, a[href='#top'], a[href='#home'], a[href='#artists'], a[href='#portfolio'], a[href='#intro'], a[href='#contact']")
    .on('click', closeAnyActiveModal);

  $('.select-language').on('change', function() {
    $("#langform").submit();
  });

  /* If you click anywhere other than 'modal-content' from modal, it close */
  $('.my-modal').on('click', function(e){
    if( $('.my-modal').hasClass('active') && $(e.target).hasClass('my-modal') )
      closeAnyActiveModal();
  });

}); /* end jQuery.ready */

function genericShowModal(event){
  	switch (event.data.name){
  		case "bio":
  			if (!isApple)
          disableScroll();
  			$("#bio-modal-" + this.dataset.id).addClass("active");
  			break;
  		case "photo":
        if (!isApple)
          disableScroll();
  			urlIndex = 0;
  			$("#photo-modal-"+this.dataset.id).addClass("active");
  			$(".left-arrow").off('click');
  			$(".right-arrow").off('click');
  			$(".left-arrow").one('click', prevImage);
  			$(".right-arrow").one('click', nextImage);
  			break;
  		case "tickets":
  			if (isMobile){
  				var win = window.open('https://shop.ticketscript.com/channel/web2/start-order/rid/RHK6NC39/language/en', '_blank');
  				win.focus();
  			} else {
  				disableScroll();
  				$("#tickets-modal").addClass("active");
  			}
  			break;
      case "lang":
        if (!isApple)
          disableScroll();
        $("#language-modal").addClass("active");
  	}
  }

  function retardedClosingEffect(){
    $(".my-modal.active").removeClass("closing");
    $(".my-modal.active").removeClass("active");
    enableScroll();
  }

  function closeAnyActiveModal(){
    $(".my-modal.active")
      .not(".no-removable")
      .addClass("closing");
    setTimeout( retardedClosingEffect, 500 );
  }

  function nextImage(){
    id = this.dataset.id - 1;
    if (arrayPreloadedImages[id].length - 1 > urlIndex)
      urlIndex++;
    else
      urlIndex = 0;

      var $active = $(".active .contentImgCarousel");

      $active.removeClass("nextImgEffect prevImgEffect");
      var $new = $active.clone();

      $active.css('background-image',
                  "url(" + arrayPreloadedImages[id][urlIndex].src + ")")
                  .before($new)
                  .addClass("nextImgEffect");
      $new.addClass("nextImgEffectClone");

      var afterEvent = function(){
          $active.removeClass("nextImgEffect");
          $new.remove();
          $(".left-arrow").off('click');
          $(".right-arrow").off('click');
          $(".left-arrow").one('click', prevImage);
          $(".right-arrow").one('click', nextImage);
      };

      if($("html").hasClass("no-flexbox")){
        setTimeout( afterEvent, 500);
      }else{
          $active.one(animEndEventName, afterEvent);
      }
  }

  function prevImage(){
    id = this.dataset.id - 1;
    if (urlIndex > 0)
      urlIndex--;
    else
      urlIndex = arrayPreloadedImages[id].length - 1;

    var $active = $(".active .contentImgCarousel");

    $active.removeClass("nextImgEffect prevImgEffect");
    var $new = $active.clone();

    $active.css("background-image",
      "url("+arrayPreloadedImages[id][urlIndex].src+")");

    $active.before($new);
    $active.addClass("prevImgEffect");
    $new.addClass("prevImgEffectClone");

    var afterEvent = function(){
        $active.removeClass("prevImgEffect");
        $new.remove();
        $(".left-arrow").off('click');
        $(".right-arrow").off('click');
        $(".left-arrow").one('click', prevImage);
        $(".right-arrow").one('click', nextImage);
    };
    if($("html").hasClass("no-flexbox")){
        setTimeout( afterEvent, 500);
    }else{
        $active.one(animEndEventName, afterEvent);
    }
  }

  function disableScroll(){
    document.body.style.overflowY = "hidden";
    //alert("of-Y hidden");
  }

  function enableScroll(){
    document.body.style.overflowY = "auto";
    //alert("of-Y auto");
  }


  function setVideo(){
    var video =
      "<video id='video_background' class='my-video' " +
      " preload='auto' autoplay='true' loop='true'> " +
        "<source src='video/video.mp4' type='video/mp4'>" +
        "<source src='video/video.ogv' type='video/ogg'>" +
        "<source src='video/video.webm' type='video/webm'>" +
      "</video>";
    $(".container-full-height .video-wrap").prepend(video);
    console.log("fiquem vídeo");
  }

  function calculateVideoMetaData(){
    $("#video_background").on("loadedmetadata", function () {
      videoSourceWidth = this.videoWidth;
      videoSourceHeight = this.videoHeight;
      ratioVideoSourceXY = videoSourceWidth / videoSourceHeight;
      resizeVideo();
			//window.addEventListener('orientationchange', resizeVideo);
			window.onresize = resizeVideo;
    });
  }

	var timeout;
  function resizeVideo(){
		clearTimeout(timeout);
		timeout = setTimeout(function(){
			var widthToVideo, leftToVideo = 0;
			var heightToVideo, topToVideo = 0;
			var bodyWidth = $(window).innerWidth();
			var bodyHeight = $(window).innerHeight();

			heightToVideo = bodyHeight;
			widthToVideo = heightToVideo * ratioVideoSourceXY;

			if (widthToVideo < bodyWidth){
				widthToVideo = bodyWidth;
				heightToVideo = widthToVideo / ratioVideoSourceXY;
				topToVideo = - ( (heightToVideo - bodyHeight) / 2);
			} else {
				leftToVideo = - ( (widthToVideo - bodyWidth) / 2);
			}

			var opts= {
				"width":widthToVideo,
				"height":heightToVideo,
				"top":topToVideo,
				"left":leftToVideo
			};
			$("#video_background").css(opts);
			console.log(videoSourceWidth+", "+videoSourceHeight + " REAL video xy");
			console.log(bodyWidth+", "+bodyHeight + " monitor");
		},100);
  }

  function insertBGImage(){
    $(".container-full-height .video-img")
      .addClass("center-bg")
      .css("background-image", "url('img/artists/joan/gallery/joan10_tiny.jpg')");
  }

  function detectDevices(){
    	// device detection
    	if (/iPhone|iPad|iPod/i.test(navigator.userAgent || navigator.vendor || window.opera))
    		isApple = true;
    	// and if is simply non-desktop device
    	if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
    		|| /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4)))
    	{
    		isMobile = true;
      }
  }



  function decidePutVideoOrImage(){

		if ($.browser.mozilla || ($.browser.webkit && !isMobile) && !isApple){
			if (!videoExists()){
				setVideo();
				calculateVideoMetaData();
			}
		} else if ($.browser.webkit || $.browser.safari || $.browser.opera || $.browser.msie || isApple) {
			if (videoExists())
				removeVideo();
			console.log("no-mozilla, ficarem foto");
			insertBGImage();
		}

  }

  function videoExists(){
		if ( $(".container-full-height .video-wrap video").length )
			return true;
		else
			return false;
	}

  function removeVideo(){
		$(".container-full-height .video-wrap video").remove();
		console.log("esborrem vídeo");
	}

  $(window).load(function(){
    decidePutVideoOrImage();
  });
