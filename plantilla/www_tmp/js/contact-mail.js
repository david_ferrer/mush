
var $form,
allOk,
fieldsToCheckList,
name,
mail,
subject,
comment,
url,
selectedLanguage,
confirm_msg,
error_msg;

$("#contact-form").submit(function (event){
  selectedLanguage = $(".select-language option[selected]").val();
  event.preventDefault();

  // Reset Globals Variabes
  $form = $( this );
  allOk = true;
  fieldsToCheckList = "";
  name = $form.find( "input[name='name']" ).val();
  mail = $form.find( "input[name='mail']" ).val();
  subject = $form.find( "input[name='subject']" ).val();
  comment = $form.find( "textarea[name='comment']" ).val();
  url = $form.attr( "action" );

  checkFields();

  if (allOk){
    showLoadingMask();

    var posting = $.post( url, {
      name: name,
      mail: mail,
      subject: subject,
      comment: comment
    } );

    posting.done(function( data ) {
      closeLoadingMask();
      //console.log("posting.done(): " + data);     // To view data from jQuery.post().done()
      if (data === "true"){
        if (selectedLanguage === "es")
          confirm_msg = "Email enviado";
        else
          confirm_msg = "Email Sent";
        showConfirmMsg(confirm_msg);
      } else {
        if (selectedLanguage === "es")
          error_msg = "Ha ocurrido un error durante el envío, por favor inténtelo más tarde";
        else
          error_msg = "An error occurred during sending, please try again later";
        showErrorMsg(error_msg);
      }
    });

  } else {
    if (selectedLanguage === "es")
      error_msg = "Por favor compruebe: <BR>" + fieldsToCheckList;
    else
      error_msg = "Please check: <BR>" + fieldsToCheckList;
    showErrorMsg(error_msg);
  }

});

function showConfirmMsg(confirm_msg){
  $("#confirm-mail-modal")
  .find(".confirm-text")
  .html(confirm_msg);

  $("#confirm-mail-modal")
    .find(".ok-email-icon")
    .css("display", "block");

  $("#confirm-mail-modal")
    .find(".error-email-icon")
    .css("display", "none");

  $("#contact-form")[0].reset();

  document.body.style.overflowY = "hidden";
  $("#confirm-mail-modal").addClass("active");
}

function showErrorMsg(error_msg){
  $("#confirm-mail-modal")
  .find(".confirm-text")
  .html(error_msg);

  $("#confirm-mail-modal")
    .find(".ok-email-icon")
    .css("display", "none");

  $("#confirm-mail-modal")
    .find(".error-email-icon")
    .css("display", "block");

  document.body.style.overflowY = "hidden";
  $("#confirm-mail-modal").addClass("active");
}

function showLoadingMask(){
  $("#loading-mail-modal .loader").css("display", "block");
  document.body.style.overflowY = "hidden";
  $("#loading-mail-modal").addClass("active");
}

function closeLoadingMask(){
  $("#loading-mail-modal .loader").css("display", "none");
  document.body.style.overflowY = "auto";
  $("#loading-mail-modal").removeClass("active");
}

function checkFields(){
  // Check if any field are empty or email is invalid
  if (name == "") {
    allOk = false;
    if (selectedLanguage === "es"){
      $form.find( "input[name='name']" ).attr("placeholder", "Nombre es requerido*");
      fieldsToCheckList += "· Nombre <BR>";
    } else {
      $form.find( "input[name='name']" ).attr("placeholder", "Name is required*");
      fieldsToCheckList += "· Name <BR>";
    }
  }
  if (mail == "" || !validateEmail(mail)){
    allOk = false;
    if (selectedLanguage === "es"){
      $form.find( "input[name='mail']" ).attr("placeholder", "E-mail es requerido*");
      fieldsToCheckList += "· E-mail <BR>";
    } else {
      $form.find( "input[name='mail']" ).attr("placeholder", "Mail is required*");
      fieldsToCheckList += "· Mail <BR>";
    }
  }
  if (subject == "") {
    allOk = false;
    if (selectedLanguage === "es"){
      $form.find( "input[name='subject']" ).attr("placeholder", "Asunto es requerido*");
      fieldsToCheckList += "· Asunto <BR>";
    } else {
      $form.find( "input[name='subject']" ).attr("placeholder", "Subject is required*");
      fieldsToCheckList += "· Subject <BR>";
    }
  }
  if (comment == "") {
    allOk = false;
    if (selectedLanguage === "es"){
      $form.find( "textarea[name='comment']" ).attr("placeholder", "Mensaje es requerido*");
      fieldsToCheckList += "· Mensaje <BR>";
    } else {
      $form.find( "textarea[name='comment']" ).attr("placeholder", "Message is required*");
      fieldsToCheckList += "· Message <BR>";
    }
  }

  function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }
}
