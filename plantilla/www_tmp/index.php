<!DOCTYPE html>
<html>
<head>
  <title>Spanish Guitar Barcelona Spain</title>
  <meta charset="utf-8">
</head>
<body>
<?php
	$lang_server = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
  $lang_form;

  if ($_POST && isset($_POST['lang'])){
    $lang_form = $_POST['lang'];
    if ($lang_form == 'es'){
      //echo "<script>alert('indiques que vols ESPAÑOL')</script>";
      include("index_es.html");
    } else if ($lang_form == 'en'){
      //echo "<script>alert('indiques que vols ENGLISH')</script>";
      include("index_en.html");
    } else {
      include("index_en.html");
    }
  } else if ($lang_server === "es" || // español
		$lang_server === "ca" || // catalán
		$lang_server === "gl" || // gallego
		$lang_server === "eu" || // euskera
		$lang_server === "an" || // aragonés
		$lang_server === "ast") // asturiano ast (ISO ISO 639-2(B))
	{
		//echo "<script>alert('lang_server [es]')</script>";
		include("index_es.html");
	} else {
		//echo "<script>alert('No español, EN')</script>";
		include("index_en.html");
	}
?>
</body>
</html>
